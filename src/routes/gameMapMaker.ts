import type { Game } from './game';

export class GameMapMaker {
	game: Game;

	constructor(_game: Game) {
		this.game = _game;
	}

	makeChessBoard() {
		for (let i = 0; i < this.game.nbRows; i++) {
			for (let j = 0; j < this.game.nbColumns; j++) {
				if ((i + j) % 2 === 0) {
					this.game.setAlive(this.game.getIndexFromCoordinates(i, j), true);
				}
			}
		}
	}

	makeGun() {
		const minWidth = 38;
		const minHeight = 15;
		if (this.game.nbColumns < minWidth || this.game.nbRows < minHeight) {
			alert(`need more than ${minWidth} columns and ${minHeight} rows for this pattern`);
			return;
		}
		const vShift = Math.floor((this.game.nbRows - minHeight) / 2);
		const hShift = Math.floor((this.game.nbColumns - minWidth) / 2);

		// left square
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 1 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 2 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 1 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 2 + hShift), true);

		// right square
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 36 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 36 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 35 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 35 + hShift), true);

		// smiley
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 11 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 11 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(9 + vShift, 11 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(10 + vShift, 12 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 12 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 13 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 14 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(11 + vShift, 13 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(11 + vShift, 14 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 15 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 17 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 18 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 17 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(9 + vShift, 17 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 16 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(10 + vShift, 16 + hShift), true);

		// hat
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 21 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 21 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 21 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 22 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 22 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 22 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 23 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 23 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 25 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 25 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(3 + vShift, 25 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(9 + vShift, 25 + hShift), true);
	}
}
