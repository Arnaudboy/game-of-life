# Game of life

Le [jeu de la vie de conway](https://conwaylife.com/) est un jeu de simulation avec zéro joueurs. Qui consiste à faire évoluer une grille de cellules par itérations successives.

À chaque itération les cellules de la grille ne peuvent être que dans un des deux états possibles: vivante (case noire) ou morte (case blanche).

Entre deux itérations chaque cellule va changer d'état en fonction de l'état des 8 cellules qui lui sont adjacentes (bas, haut, gauche, droite, diagonales bas gauche, bas droit, haut droit, haut gauche).

Les règles d'évolution sont les suivantes:

- Une cellule vivante avec moins de deux voisines vivantes meurt (underpopulation).
- Une cellule vivante avec plus de trois voisines vivantes meurt (overpopulation).
- Une cellule vivante avec deux ou trois voisines vivantes reste en vie (pas de changement) pour la génération suivante
- Une cellule morte avec exactement trois voisines vivantes devient vivante au tour suivant

Votre travail consitera à implémenter cette logique dans le fichier simulator.ts fourni.

## Compiler & télécharger les dépendances

```bash
npm install
```

## Lancer l'application

```bash
npm run dev
```

Puis rendez-vous dans votre navigateur: http://localhost:5173/
